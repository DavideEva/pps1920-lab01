package lab01.example.model;

public abstract class SimpleBankAccountAbstract implements BankAccount {

    private double balance;
    private final AccountHolder holder;

    public SimpleBankAccountAbstract(final AccountHolder holder, final double balance) {
        this.holder = holder;
        this.balance = balance;
    }

    @Override
    public AccountHolder getHolder() {
        return this.holder;
    }

    @Override
    public double getBalance() {
        return this.balance;
    }

    @Override
    public abstract void deposit(int usrID, double amount);

    @Override
    public abstract void withdraw(int usrID, double amount);

    protected void setBalance(double balance) {
        this.balance = balance;
    }

    protected boolean checkUser(final int id) {
        return this.holder.getId() == id;
    }

}
