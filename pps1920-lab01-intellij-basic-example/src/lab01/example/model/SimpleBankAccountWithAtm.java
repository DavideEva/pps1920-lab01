package lab01.example.model;

public class SimpleBankAccountWithAtm extends SimpleBankAccount implements BankAccountWithAtm {

    public static final int ATM_FEE = 1;
    private double ATMFee;

    public SimpleBankAccountWithAtm(AccountHolder holder, double balance, double ATMFee) {
        super(holder, balance);
        this.ATMFee = ATMFee;
    }

    public SimpleBankAccountWithAtm(AccountHolder holder, double balance) {
        this(holder, balance, ATM_FEE);
    }

    @Override
    public void depositWithAtm(int usrID, double amount) {
        this.setBalance(this.getBalance() + amount - this.ATMFee);
    }

    @Override
    public void withdrawWithAtm(final int usrID, final double amount) {
        if (this.checkUser(usrID) && isWithdrawAllowed(amount)) {
            this.setBalance(this.getBalance() - amount - this.ATMFee);
        }
    }

    private boolean isWithdrawAllowed(final double amount) {
        return this.getBalance() + this.ATMFee > amount;
    }
}
