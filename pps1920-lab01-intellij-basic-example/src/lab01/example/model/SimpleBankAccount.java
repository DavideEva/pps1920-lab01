package lab01.example.model;

/**
 * This class represent a particular instance of a BankAccount.
 * In particular, a Simple Bank Account allows always the deposit
 * while the withdraw is allowed only if the balance greater or equal the withdrawal amount
 */
public class SimpleBankAccount extends SimpleBankAccountAbstract {

    public SimpleBankAccount(final AccountHolder holder, final double balance) {
        super(holder, balance);
    }

    @Override
    public void deposit(final int usrID, final double amount) {
        if (checkUser(usrID)) {
            this.setBalance(this.getBalance() + amount);
        }
    }

    @Override
    public void withdraw(final int usrID, final double amount) {
        if (this.checkUser(usrID) && isWithdrawAllowed(amount)) {
            this.setBalance(this.getBalance() - amount);
        }
    }

    private boolean isWithdrawAllowed(final double amount){
        return this.getBalance() >= amount;
    }
}
