import lab01.example.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleBankAccountWithAtmTest {

    private AccountHolder accountHolder;
    private BankAccountWithAtm bankAccountWithAtm;

    @BeforeEach
    void setUp() {
        accountHolder = new AccountHolder("Mario", "Rossi", 1);
        bankAccountWithAtm = new SimpleBankAccountWithAtm(accountHolder, 0);
    }

    @Test
    void testDepositWithAtm() {
        bankAccountWithAtm.depositWithAtm(accountHolder.getId(), 100);
        assertEquals(99, bankAccountWithAtm.getBalance());
    }

    @Test
    void testWithdrawWithAtm() {
        bankAccountWithAtm.depositWithAtm(accountHolder.getId(), 100);
        // balance = 99

        bankAccountWithAtm.withdrawWithAtm(accountHolder.getId(), 50);
        // balance = 48

        assertEquals(48, bankAccountWithAtm.getBalance());
    }

    @Test
    void testFailWithdrawWithAtm() {
        bankAccountWithAtm.depositWithAtm(accountHolder.getId(), 50);

        //balance = 49

        bankAccountWithAtm.withdrawWithAtm(accountHolder.getId(), 50);

        assertEquals(49, bankAccountWithAtm.getBalance());
    }
}