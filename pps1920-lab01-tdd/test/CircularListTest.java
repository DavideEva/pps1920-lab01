import lab01.tdd.CircularList;
import lab01.tdd.SelectStrategyFactory;
import lab01.tdd.SimpleCircularList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    //Should be lower than 42
    public static final int NEXT_WITH_STRATEGY_FAIL_STEP = 10;
    public static final int MULTIPLE_NEXT_FILL = 10;
    public static final int MULTIPLE_NEXT = 5;
    public static final int NEXT_CIRCULAR_STEP = 10;
    public static final int MULTIPLE_PREVIOUS = 10;
    public static final int MULTIPLE_PREVIOUS_STEP = 5;
    //Should be greater than 5
    public static final int NEXT_WITH_STRATEGY_STEP = 10;
    private CircularList circularList;

    @BeforeEach
    void setUp() {
        this.circularList = new SimpleCircularList();
    }

    @Test
    void testAdd() {
        this.circularList.add(1);

        assertFalse(this.circularList.isEmpty());
    }

    @Test
    void testSize() {
        this.circularList.add(1);

        assertEquals(this.circularList.size(), 1);
    }

    @Test
    void testIsEmpty() {
        assertTrue(this.circularList.isEmpty());
    }

    @Test
    void testNext() {
        this.circularList.add(1);
        assertEquals(1, this.circularList.next().get());
    }

    @Test
    void testNextFail() {
        assertEquals(this.circularList.next(), Optional.empty());
    }

    @Test
    void testMultipleNext() {
        for (int i = 0; i < MULTIPLE_NEXT_FILL; i++) {
            this.circularList.add(i);
        }

        for (int i = 0; i < MULTIPLE_NEXT; i++) {
            this.circularList.next();
        }

        assertEquals(MULTIPLE_NEXT_FILL - MULTIPLE_NEXT, this.circularList.next().get());
    }

    @Test
    void testNextCircularity() {
        for (int i = 0; i < NEXT_CIRCULAR_STEP; i++) {
            this.circularList.add(i);
        }

        for (int i = 0; i < NEXT_CIRCULAR_STEP; i++) {
            this.circularList.next();
        }

        assertEquals(0, this.circularList.next().get());
    }

    @Test
    void testPrevious() {
        this.circularList.add(1);
        assertEquals(1, this.circularList.previous().get());
    }

    @Test
    void testMultiplePrevious() {
        for (int i = 0; i < MULTIPLE_PREVIOUS; i++) {
            this.circularList.add(i);
        }

        for (int i = 0; i < MULTIPLE_PREVIOUS_STEP; i++) {
            this.circularList.previous();
        }

        assertEquals(MULTIPLE_PREVIOUS - MULTIPLE_PREVIOUS_STEP, this.circularList.previous().get());
    }

    @Test
    void testReset() {
        this.circularList.add(1);
        this.circularList.add(2);
        this.circularList.add(3);
        this.circularList.next();

        this.circularList.reset();

        assertEquals(1, this.circularList.next().get());
    }

    @Test
    void testNextWithStrategy() {
        for (int i = 0; i < NEXT_WITH_STRATEGY_STEP; i++) {
            this.circularList.add(i);
        }

        assertEquals(4, this.circularList.next(SelectStrategyFactory.equalsStrategy(4)).get());
    }

    @Test
    void testFailNextWithStrategy() {
        for (int i = 0; i < NEXT_WITH_STRATEGY_FAIL_STEP; i++) {
            this.circularList.add(i);
        }

        assertEquals(Optional.empty(), this.circularList.next(SelectStrategyFactory.equalsStrategy(42)));
    }
}
