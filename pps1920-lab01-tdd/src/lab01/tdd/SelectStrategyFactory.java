package lab01.tdd;

public class SelectStrategyFactory {

    public static SelectStrategy evenStrategy() {
        return e -> e % 2 == 0;
    }

    public static SelectStrategy multipleOfStrategy(int element) {
        return e -> e % element == 0;
    }

    public static SelectStrategy equalsStrategy(int element) {
        return e -> e == element;
    }
}
