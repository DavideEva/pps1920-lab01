package lab01.tdd;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class SimpleCircularList implements CircularList {

    private List<Integer> list;
    private Integer pos;

    public SimpleCircularList() {
        this.list = new ArrayList<>();
        this.pos = 0;
    }

    @Override
    public void add(int element) {
        this.list.add(element);
    }

    @Override
    public int size() {
        return this.list.size();
    }

    @Override
    public boolean isEmpty() {
        return this.list.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        if (!this.isEmpty()) {
            int value = list.get(this.pos);
            this.pos = (this.pos + 1) % this.size();
            return Optional.of(value);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Integer> previous() {
        if (!this.isEmpty()) {
            int value = list.get(this.pos);
            this.pos = (this.pos - 1 + this.size()) % this.size();
            return Optional.of(value);
        }
        return Optional.empty();
    }

    @Override
    public void reset() {
        this.pos = 0;
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        return Stream.generate(this::next)
                .limit(this.size())
                .map(Optional::get)
                .filter(strategy::apply)
                .findFirst();
    }
}
